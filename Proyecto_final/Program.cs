﻿using System;

namespace Proyecto_final
{
    class vector
    {   
        private string[] empleados;
        private int[,] sueldos;
        private int[] salariotot;

        public void Cargar()
        {
            Console.WriteLine("<<<<<< Bienvenido a mi proyecto final >>>>>>");
            Console.WriteLine();
         
            empleados = new String[5];
            sueldos = new int[5, 3];
            for (int f = 0; f < empleados.Length; f++)
            {
                Console.Write("Ingrese el nombre del operario " + (f + 1) + ": ");
                empleados[f] = Console.ReadLine();
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    Console.Write("Ingrese sueldo " + (c + 1) + ": ");
                    string linea;
                    linea = Console.ReadLine();
                    sueldos[f, c] = int.Parse(linea);
                }
            }
        }

        public void salarioacumulado()
        {
            salariotot    = new int[5];
            for (int f = 0; f < sueldos.GetLength(0); f++)
            {
                int suma = 0;
                for (int c = 0; c < sueldos.GetLength(1); c++)
                {
                    suma = suma + sueldos[f, c]; 
                }
                salariotot[f] = suma;

            }
        }

        public void ImprimirTotalPagado()
        {
            Console.WriteLine("EL Total de sueldos pagados a los empleados:");
            for (int f = 0; f < salariotot.Length; f++)
            {
                Console.WriteLine(empleados[f] + " obtuvo  " + salariotot[f] + " durante los tres meses. ");
            }
        }

        public void EmpleadoMayoringreso()
        {
            int may = salariotot[0];
            string nom = empleados[0];
            for (int f = 0; f < salariotot.Length; f++)
            {
                if (salariotot[f] > may)
                {
                    may = salariotot[f];
                    nom = empleados[f];
                }
            }
            Console.WriteLine("El operario con mayor sueldo es " + nom + " que tiene un total de " + may + " pesos");
        }

        static void Main(string[] args)
        {
            vector vec = new vector();
            vec.Cargar();
            vec.salarioacumulado();
            vec.ImprimirTotalPagado();
            vec.EmpleadoMayoringreso();
            Console.ReadKey();
        }
    }
}
   

